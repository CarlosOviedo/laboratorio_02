from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-26/Ejercicio_01 - Combox.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.lista.currentIndexChanged.connect(self.on_lista_changed)

    def on_lista_changed (self, index):
        indice = self.lista.currentIndex()
        self.eleccion.setText(str (indice) + " - " + self.lista.currentText())
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  