from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-26/Ejercicio_03 - ProgressBar.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.progreso.valueChanged.connect(self.on_progreso)
        self.descarga.clicked.connect (self.on_descargar)

    def on_progreso (self, index):
        self.etiqueta.setText("VALOR : " + str(self.progreso.value()))
    
    def on_descargar (self, index):
        descarga = 0.0
        while descarga < 100.0:
            descarga +=0.00001
            self.progreso.setValue(descarga)

app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  