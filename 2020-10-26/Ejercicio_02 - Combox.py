from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog, QMessageBox
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-26/Ejercicio_02 - Combox.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.indice = -1
        self.lista.currentIndexChanged.connect(self.on_lista_changed)
        self.agregar.clicked.connect            (self.On_agregar)
        self.editar.clicked.connect             (self.On_editar)
        self.quitar_item.clicked.connect        (self.On_quitar_item)
        self.quitar_todos.clicked.connect       (self.On_quitar_todos)
    
    def On_agregar (self):
        nuevo_texto, ok = QInputDialog.getText(self,'Agregar','Ingrese texto',text="")
        if ok:
            self.lista.addItem(nuevo_texto)
        
    def On_quitar_item (self):
        texto_item = self.lista.currentText()
        msg = QMessageBox()
        #Titulo
        msg.setWindowTitle("Quitar Item")
        #Cuerpo
        msg.setText("Desea quitar el item: " + texto_item)
        #Icono
        msg.setIcon(QMessageBox.Warning)

        #Botones
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No )
        respuesta = msg.exec_() 
        if respuesta == QMessageBox.Yes:
            print ("Se elijio SI")
            self.lista.removeItem(self.indice)
        elif respuesta == QMessageBox.No:
            print ("Se elijio NO")            
        self.eleccion.setText(str (self.indice) + " - " + self.lista.currentText())
        
    
    def On_quitar_todos (self):
        texto_item = self.lista.currentText()
        msg = QMessageBox()
        #Titulo
        msg.setWindowTitle("Quitar Todos")
        #Cuerpo
        msg.setText("Desea quitar todos los items: ")
        #Icono
        msg.setIcon(QMessageBox.Warning)

        #Botones
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No )
        respuesta = msg.exec_() 
        if respuesta == QMessageBox.Yes:
            print ("Se elijio SI")
            self.lista.clear()
        elif respuesta == QMessageBox.No:
            print ("Se elijio NO")          
        self.eleccion.setText("")
    
    def On_editar (self):
        texto_item = self.lista.currentText()
        msg = QMessageBox()
        #Titulo
        msg.setWindowTitle("Editar Item")
        #Cuerpo
        msg.setText("¿Desea Editar: " + texto_item +" ?")
        #Icono
        msg.setIcon(QMessageBox.Warning)

        #Botones
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No )
        
  
        nuevo_texto, ok = QInputDialog.getText(self,'Editar','Ingrese un nuevo Texto',text=texto_item)    
        if ok:
            respuesta = msg.exec_() 
            if respuesta == QMessageBox.Yes:
                print ("Se elijio SI")
                self.lista.setItemText(self.indice,nuevo_texto)
            elif respuesta == QMessageBox.No:
                print ("Se elijio NO")  
        self.eleccion.setText(str (self.indice) + " - " + self.lista.currentText())    
    
    def on_lista_changed (self, index):
        self.indice = self.lista.currentIndex()
        if self.indice != -1:
            self.eleccion.setText(str (self.indice) + " - " + self.lista.currentText())
           
        else:
            self.eleccion.setText("")

   
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  