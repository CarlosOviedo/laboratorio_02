from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-19/Ejercicio_03 - QListWidget.ui",self)     #Ubicamos la ventana realizado con "designed"
        self.agregar.clicked.connect            (self.On_agregar)
        self.editar.clicked.connect             (self.On_editar)
        self.quitar_item.clicked.connect        (self.On_quitar_item)
        self.quitar_todos.clicked.connect       (self.On_quitar_todos)
        self.lista.itemDoubleClicked.connect    (self.On_editar)

    def On_agregar (self):
        self.lista.addItem(self.nombre.text())
        self.nombre.clear()
    
    def On_quitar_item (self):
        self.lista.takeItem(self.lista.currentRow())
    
    def On_quitar_todos (self):
        self.lista.clear()
    
    def On_editar (self):
        texto_item = self.lista.currentItem().text()
        nuevo_texto, ok = QInputDialog.getText(self,'Editar','Ingrese nuevo nombre',text=texto_item)    
        if ok:
            self.lista.currentItem().setText(nuevo_texto)






app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  