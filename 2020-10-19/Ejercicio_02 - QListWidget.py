from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-19/Ejercicio_02 - QListWidget.ui",self)     #Ubicamos la ventana realizado con "designed"
        self.lista.itemSelectionChanged.connect(self.on_item_changed)  #Otra Opcion
        


    def on_item_changed(self):
        self.seleccion.clear()
        items = self.lista.selectedItems()
        for item in items:
            self.seleccion.addItem(item.text())





app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  