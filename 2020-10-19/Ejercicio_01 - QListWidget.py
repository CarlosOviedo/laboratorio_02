from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-19/Ejercicio_01 - QListWidget.ui",self)     #Ubicamos la ventana realizado con "designed"
        #self.lista.itemClicked.connect(self.on_item_clicked)           #PrimeraOpcion
        #self.lista.itemSelectionChanged.connect(self.on_item_changed)  #Otra Opcion
        self.lista.currentItemChanged.connect(self.on_item_changed)     #Tenemos las opciones anterior tambien


#   def on_item_clicked(self):
#       self.seleccion.setText(self.lista.currentItem().text())
#       print ("Item Clickeado")

#   def on_item_changed(self):
#       self.seleccion.setText(self.lista.currentItem().text())
#       print ("Item Clickeado")

    def on_item_changed(self,actual,anterior):
        self.seleccion.setText(actual.text())
        if anterior:                            #Si no hay ninguno previo no muestra
            print ("Anterior: ",anterior.text(),"Actual: ",actual.text())



app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  