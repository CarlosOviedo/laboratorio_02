#Se puede hacer cadenas con ' o ". La diferencia de uso 
#puede ser utilizado para poner " o ' en el texto.
#Tambien como en C se puede utilizar \

# Cadenas de Lineas Simple:
cadena1 = 'Una "Cadena" de texto'
cadena2 = "Una 'Cadena' de texto"
cadena3 = "Una \"Cadena\" de texto"

# Cadenas de Multiples Lineas:
cadena4 = '''Una Cadena
de
varias
lieas '''
cadena5 = """Una Cadena
de
varias
lieas """

print (cadena1)
print (cadena2)
print (cadena3)
print (cadena4)
print (cadena5)