#Los Diccionarios, se inicilizan con llaves

dic = {
    "banana": "Amarilla",
    "manzana": "Roja",
    "pera": "Verde"
    }
#Imprime Todo el Dic.
print (dic)
#Imprime el item que querramos
print (dic["banana"])
print (dic.items())
#Modificar el valor de un item.
dic["banana"]= 25
print (dic)
#Devuelve los valores
print (dic.values())
#Devuelve las llaves
print (dic.keys())
#Eliminar un item
print (dic.pop("manzana"))
print (dic)