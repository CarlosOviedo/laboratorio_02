cadena = "es texto"
Titulo = "Es Un Titulo"
num = 14
#Imprime el tipo de variable
print(type (cadena))
print(type (num))

#Imprime las funciones que se puede hacer con esta variable
print (dir(cadena))
#Ejemplo de una funcion de la variable cadena.
print (cadena.upper())    # Cambia el texto a Mayuscula
print (cadena.isalpha())  # Consulta si es Alphanumerico
print (Titulo.istitle())  #Indica si es un titulo (Todas las palabras en Mayus)
print (cadena.replace("es", "ES")) #Remplaza 
print (cadena.index("t")) #Busca algo
print (cadena.count("e")) # Cuenta cuantas letras e tiene
print(len (cadena))       # Cuenta cuantas letras tiene
print(cadena[-2]) # Imprime una letra de la cadena, si es negativo empieza de derecha a izq