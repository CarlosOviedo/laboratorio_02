# Similares a los vectores, pero pueden tener distintos tipos de variables
# Enteros, Float, Char , otra lista, etc

lista1 = [0, 5, 2, 5, -1, 10, 3, 2]

print (lista1)
print (lista1[5]) # Imprime el 5to elemento
print (lista1[-2]) #Imprime el 2do elemento contando desde la derecha

#Operador :
print (lista1 [1:4]) #1 incio 4 fin
print (lista1 [:4])
print (lista1 [1:])

print (lista1 [-3:-1])
print (lista1 [-3:])
print(lista1[1:6:2]) #1 incio 6 paso 2 fin

