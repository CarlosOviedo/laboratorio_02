
lista1 = [0, 5, 2, 5, -1, 10, 3, 2]
lista2 = [4, 6]

print(lista1)
#Tamaño de la lista
print(len(lista1))

#Agregar un elemento al final
lista1.append(45)
print(len(lista1))
print(lista1)

#Agregar en alguna posicion
lista1.insert(2,66) #insert(posicion,dato)
print(len(lista1))
print(lista1)

#Diferencia entre append + extend
lista1.append(lista2)
print(len(lista1))
print(lista1)
print(lista1 + lista2)

lista1.extend([-3,-7])

#Pone al reves la lista
lista1.reverse()
print(lista1)

#Quitar un elemento segun el indice
lista1.pop(2)
print(lista1)

#Elimina un elemento. El primero que encuentra
lista1.pop(3)
print(lista1)

#Ordena las lista de manera asc.
lista1.sort()
print(lista1)

#Ordena las lista de manera dsc.
lista1.sort(reverse=True)
print(lista1)

