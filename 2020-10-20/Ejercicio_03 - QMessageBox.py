from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog, QMessageBox
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-20/Ejercicio_03 - QMessageBox.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.mensaje.clicked.connect           (self.on_clicked)

    def on_clicked (self):
        msg = QMessageBox()
        #Titulo
        msg.setWindowTitle("Titulo")
        #Cuerpo
        msg.setText("Esto es un mensaje")
        #Icono
        msg.setIcon(QMessageBox.Information)
        #msg.setIcon(QMessageBox.Critical)
        #msg.setIcon(QMessageBox.Question)
        #msg.setIcon(QMessageBox.Warning)
        #Botones
        #msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel | QMessageBox.Open | QMessageBox.Close | QMessageBox.SaveAll | QMessageBox.Abort | QMessageBox.Retry | QMessageBox.Ignore )
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        respuesta = msg.exec_() 
        if respuesta == QMessageBox.Yes:
            print ("Se elijio SI")
        elif respuesta == QMessageBox.No:
            print ("Se elijio NO")            
        else:
            print ("Se elijio Cancelar")

    


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  