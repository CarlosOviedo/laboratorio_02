from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-20/Ejercicio_01 - QLineEdit.ui",self)               #Ubicamos la ventana realizado con "designed"
        self.ingresar.clicked.connect           (self.on_clicked)

    def on_clicked (self):
        #Texto:
        #texto, ok = QInputDialog.getText(self,'Ingresar','Ingrese un texto')    
        #if ok and texto:
        #    self.entrada.setText(texto)
        #Entero
        #entero, ok = QInputDialog.getInt(self,'Ingresar','Ingrese un texto',value=8,min=0,max=100,step=2)
        #if ok:
        #    self.entrada.setText(str(entero))
        # Double:
        #decimal, ok = QInputDialog.getDouble(self,'Ingresar','Ingrese un texto',1.5,0,100)
        #if ok:
        #    self.entrada.setText(str(decimal))
        # Lista:
        items = ['Rojo','Verde','Azul']
        item, ok = QInputDialog.getItem(self,'Ingresar','Elija un Item',items,0,False)
        if ok:
            self.entrada.setText(item)

    


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  