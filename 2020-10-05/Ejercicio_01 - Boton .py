from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                    #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                 #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                          #Inicializamos Clase
        super().__init__()                                       #Inicializamos Clase Superior: (Objeto Padre)
                                                                 #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-05/Ejercicio_01 - Boton.ui",self)    #Ubicamos la ventana realizado con "designed"
        self.boton.clicked.connect(self.on_clicked)              #Conectar un evento con una funcion.


    def on_clicked(self):
        self.etiqueta.setText("Chau Mundo")


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()