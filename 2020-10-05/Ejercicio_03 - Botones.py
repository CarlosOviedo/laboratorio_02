from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                             #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                      #Inicializamos Clase
        super().__init__()                                                   #Inicializamos Clase Superior: (Objeto Padre)
                                                                             #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-05/Ejercicio_03 - Botones.ui",self)    #Ubicamos la ventana realizado con "designed"
        self.Boton_01.clicked.connect(self.on_clicked_Boton_01)
        self.Boton_02.clicked.connect(self.on_clicked_Boton_02)                             #Conectar un evento con una funcion.


    def on_clicked_Boton_01(self):
       self.Boton_02.setEnabled(True)
       self.Boton_01.setEnabled(False)

    def on_clicked_Boton_02(self):
       self.Boton_02.setEnabled(False)
       self.Boton_01.setEnabled(True)


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()