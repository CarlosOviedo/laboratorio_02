from clases.Classcancion       import Cancion
from clases.Classcanciones     import Lista

cancion_01 = Cancion ('Cancion_01' , 'Interprete_01' , 3 , 'Estilo_01')
cancion_02 = Cancion ("Cancion_02" , "Interprete_01" , 3 , "Estilo_02")
cancion_03 = Cancion ("Cancion_03" , "Interprete_02" , 2.5 , "Estilo_01")
cancion_04 = Cancion ("Cancion_04" , "Interprete_02" , 2.9 , "Estilo_03")

print ("Agregar Temas a la Lista")
ListaCanciones= Lista("Lista_01")
ListaCanciones.agregar(cancion_01)
ListaCanciones.agregar(cancion_02)
ListaCanciones.agregar(cancion_03)
ListaCanciones.agregar(cancion_04)

print ("\nLista de Canciones")
ListaCanciones.listar_todas()

print("\nLista del Interprete: 'Interprete_01'")
ListaCanciones.listar_por_interprete_lista("Interprete_01")


print("\nLista de Canciones del estilo 'Estilo_01'")
ListaCanciones.listado_por_estilo ("Estilo_01")


print ("\nQuitar elemento: ")
ListaCanciones.quitar_listado('Cancion_01','Interprete_01')

print ("\nLista de Canciones")
ListaCanciones.listar_todas()