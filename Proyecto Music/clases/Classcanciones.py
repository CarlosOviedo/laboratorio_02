# Class: Canciones.
# Parametro: listado de canciones.
# Metodo:
#   Agregar una cancion.
#   listado_por_interprete: Muestra todas las canciones de un interprete
#   listado_por_estilo: Muestra tdas las canciones de un estilo.
#   quitar: quitar una cancion por titulo e interprete
from clases.Classcancion  import Cancion

def filtro_interprete(cancion):
    if cancion.interprete==interprete:
        return cancion.resumen()

class Lista:
    def __init__(self,nombre):
        self.listado_canciones = []
        self.nombre=nombre
    
    def agregar(self,song):
        if type(song) == Cancion:
            self.listado_canciones.append(song)
            print("Se agregó la canción : "+song.titulo)
        else:
            print("No es una canción")
    
    def listar_todas(self):
        for cancion in self.listado_canciones:
            print(cancion)

    def quitar(self,titulo,interprete):
        for cancion in self.listado_canciones:
            if(cancion.titulo==titulo and cancion.interprete==interprete):
                self.listado_canciones.remove(cancion)
                print ("Se elimino: ",titulo," - ",interprete)
    
    def listar_por_interprete(self,interprete):
        for cancion in self.listado_canciones:
            if(cancion.interprete==interprete):
                print(cancion.resumen())
    

    def listar_por_interprete_lista(self,interprete):
        retornoLista = []
        for cancion in self.listado_canciones:
            retornoLista.append(list(filter(filtro_interprete,cancion))
        return retornoLista
    
    def listar_por_estilo(self,genero):
        for cancion in self.listado_canciones:
            if(type(cancion.genero)==list):
                if(cancion.genero.count(genero)>0):
                    print(cancion.resumen())
            else:
                if(cancion.genero.find(genero)!=-1 and type(cancion.genero)==str):
                    print(cancion.resumen())