edades = [13,12,16,23,24,56,45,10,78,45,39]

# A partir de la lista filtrar aquellas edades entre 18 y 36
def filtroEdad(edad):
    return 18 < edad < 36

Edades_Filtradas = list (filter(filtroEdad,edades))

#Otro Metodo:
inf = 18
sup = 36
Edades_Filtradas_2 = list (filter(lambda edad: edad in range (inf-1,sup),edades))

#Otro Metodo:
inferior = 18
superior = 36
def func_rango(edad):
    return inferior < edad < superior

Edades_Filtradas_3 = list (filter(func_rango,edades))

print ("Lista Completa: " ) 
print(edades)

print ("Lista Filtrada: ")
print (Edades_Filtradas)


print ("Lista Filtrada_2: ")
print (Edades_Filtradas_2)

print ("Lista Filtrada_3: ")
print (Edades_Filtradas_3)