numero_entero = 10
numero_decimal = 10.4
Cadena = "Texto"
boolean = True

#Primera Forma de Imprimir. En lineas diferentes
print (numero_entero)
print (numero_decimal)
print (Cadena)
#Segunda Forma de Imprimir. En la misma linea
print (numero_entero , numero_decimal , Cadena)

print ("El numero vale {0} {1} {0}".format(numero_entero,numero_decimal))
#Otra forma mas nueva de mostrar los valores de las variables
print (f"El numero vale {numero_entero} {numero_decimal} {Cadena} {boolean}")