from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                             #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                      #Inicializamos Clase
        super().__init__()                                                   #Inicializamos Clase Superior: (Objeto Padre)
                                                                             #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-08/Ejercicio_03 - RadioButton.ui",self)    #Ubicamos la ventana realizado con "designed"
        self.boton.clicked.connect(self.on_clicked_boton)

    def on_clicked_boton(self):
        texto ="Se elige las opciones "
        print("Se Apreto el Boton")
        if self.opcion1.isChecked():
            texto = texto + "opcion 01 - "
        elif self.opcion2.isChecked():
            texto = texto + "opcion 02 - "
        elif self.opcion3.isChecked():
            texto = texto + "opcion 03 - "           
        if self.opciona.isChecked():
            texto = texto + "opcion A"
        elif self.opcionb.isChecked():
            texto = texto + "opcion B"
        elif self.opcionc.isChecked():
            texto = texto + "opcion C"           
        self.etiqueta.setText(texto)        



app = QApplication([])

win = MiVentana()
win.show()
app.exec_()