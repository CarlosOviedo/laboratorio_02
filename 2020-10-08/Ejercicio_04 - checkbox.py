from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                             #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                      #Inicializamos Clase
        super().__init__()                                                   #Inicializamos Clase Superior: (Objeto Padre)
                                                                             #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-08/Ejercicio_04 - checkbox.ui",self)    #Ubicamos la ventana realizado con "designed"
        self.boton.clicked.connect(self.on_clicked_boton)

    def on_clicked_boton(self):
        calculo = 0
        texto = "Precio extras: "
        print("Se Apreto el Boton")
        if self.jamon.isChecked():
            calculo = calculo + 20
        if self.huevos.isChecked():
            calculo = calculo + 10
        if self.tomates.isChecked():
            calculo = calculo + 50           
        texto = texto + str(calculo)  
        self.precio.setText(texto)     

app = QApplication([])

win = MiVentana()
win.show()
app.exec_()        