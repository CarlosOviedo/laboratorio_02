from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                             #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                      #Inicializamos Clase
        super().__init__()                                                   #Inicializamos Clase Superior: (Objeto Padre)
                                                                             #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-08/Ejercicio_01 - RadioButton.ui",self)    #Ubicamos la ventana realizado con "designed"
        self.boton.clicked.connect(self.on_clicked_boton)


    def on_clicked_boton(self):
       if self.opcion1.isChecked():
           self.etiqueta.setText("Se elige la opcion 01")
       elif self.opcion2.isChecked():
           self.etiqueta.setText("Se elige la opcion 02")
       elif self.opcion3.isChecked():
           self.etiqueta.setText("Se elige la opcion 03")           
       else:
            self.etiqueta.setText("No se eligio opcion")


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()