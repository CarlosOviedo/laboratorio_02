# QT5 con Python ... Importamos:
from PyQt5.QtWidgets import QApplication, QMainWindow, QHBoxLayout,QLabel,QWidget

# Creamos la App
app = QApplication([])

#Creamos la ventana Principal:
ventana = QMainWindow()
ventana.setWindowTitle("Primer Programa") # Modifica el Titulo de la ventana


#Agregar Componentes:
widget = QWidget() #Creamos un Widget
layout = QHBoxLayout() #Creamos el Layout \ QHBoxLayout(): Horizontal QVBoxLayout(): Vertical
label = QLabel ("Hola Mundo") #Creamos un Label
label2 = QLabel ("Chau Mundo") #Creamos un Label

layout.addWidget(label) #Agregamos un Label
layout.addWidget(label2) #Agregamos un Label

widget.setLayout(layout) #Fijamos el layout al Widget

#Agregamos el Widget a la Ventana 
ventana.setCentralWidget(widget)
# Muestra la ventana 
ventana.show() 

#Ejecutamos la aplicacion
app.exec_() #Con el guion: permite recbir los datos del raton & teclado