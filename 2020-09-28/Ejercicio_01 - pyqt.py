# QT5 con Python ... Importamos:
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout,QLabel,QWidget

# Creamos la App
app = QApplication([])

#Creamos la ventana Principal:
ventana = QMainWindow()
ventana.setWindowTitle("Primer Programa") # Modifica el Titulo de la ventana

# Muestra la ventana 
ventana.show() 

#Ejecutamos la aplicacion
app.exec_() #Con el guion: permite recbir los datos del raton & teclado

