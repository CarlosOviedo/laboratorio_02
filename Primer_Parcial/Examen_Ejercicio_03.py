#Ejercicio 03: Crear objeto Cilindro con métodos área y volumen

from math import pi

class cilindro:
    def __init__ (self, radio,altura):
        self.radio = radio
        self.altura = altura
        print ("Objeto Circulo creado con radio: ", self.radio, " y altura: ", self.altura)
    def area (self):
        return (2 * pi * (self.radio ** 2))
    def volumen (self):
        return (pi * (self.radio ** 2)*self.altura)

# Ejemplo de Aplicacion: 
cilindro_01 = cilindro(2,2)
print ("Area: " ,cilindro_01.area())
print ("Volumen: ", cilindro_01.volumen())