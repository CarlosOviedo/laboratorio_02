#Ejercicio 04:
# Empleando mapas convertir una lista de 10 temperaturas en grados centigrados 
# a otra con temperaturas en grados Fahrenheit.

from math import pi

temp_grados = [0,10,20,25,30,55,70,85,90,100]

def Fahrenheit_func(temp):
    return (temp*(9/5) + 32)

temp_Fahrenheit = list (map (Fahrenheit_func,temp_grados))

print ("Temperatura en Grados - Temperatura en Fahrenheit")
for i in range (0,len(temp_grados)):
    print (temp_grados[i], " \t - \t\t ", temp_Fahrenheit[i])
