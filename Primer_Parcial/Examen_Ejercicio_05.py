#Ejercicio 05: Una lista tiene 10 letras.
#              Crear un filtro que devuelva una lista solo con vocales.
Lista_letras=["a","c","d","f","g","u","p","o","m","e"]

def vocales(letra):
    if letra =="a" or letra =="e" or letra =="i" or letra =="o" or letra =="u":
        return(letra)

Lista_vocales = list (filter(vocales,Lista_letras))

print ("Letras: ",Lista_letras)
print ("Vocales: ",Lista_vocales)