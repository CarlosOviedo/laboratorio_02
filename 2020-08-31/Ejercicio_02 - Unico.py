# Cargar una lista de numeros enteros
# Implentar una funcion que devuelva una nueva lista solo con los elementos unicos de la anterior.
# Ejemplo:
#           Entrada:[1,2,3,3,3,4,4,6,7,7,0]
#           Salida :[1,2,3,4,6,7,0]

#Funcion: 
def unico (lista):
    duplicado=[]
    for elemento in lista:
        if(elemento not in duplicado ):
            duplicado.append(elemento)
    return duplicado


# Funcion Principal:

lista=[] 
cnt=0
while cnt < 5:

    numero=int (input("Ingrese un numero entero: "))
    lista.append(numero)
    print(lista)
    cnt=cnt+1

print (lista)
print (unico(lista))