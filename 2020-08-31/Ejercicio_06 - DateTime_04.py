#Operaciones con DateTime

from datetime import datetime, timedelta

fecha_01="2020-08-31"
fecha_02= "2020-12-25"

dt1 = datetime.strptime(fecha_01,"%Y-%m-%d")
dt2 = datetime.strptime(fecha_02,"%Y-%m-%d")

diff = dt2-dt1
print("Falta para Navidad: ",diff)

td = timedelta(days=5)

print("Dentro de 5 Dias es: ",dt1+td)