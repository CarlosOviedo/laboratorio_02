from datetime import datetime

# Guardar en Variables:
d=datetime.now()

print("Variable: ",d)
print("Fecha - Hora : " + str (datetime.now()))

print ("Año: ",d.year)
print ("Mes: ",d.month)
print ("Dia: ",d.day)
print ("Hora: ",d.hour)
print ("Minutos: ",d.minute)
print ("Segundos: ",d.second)
print ("MicroSegundos: ",d.microsecond)
