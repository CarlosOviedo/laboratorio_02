# Pedir al usuario el radio
# Calcular diametro perimetro area
from MODULOcirculo import area,diametro,perimetro

radio = float(input("Ingrese el Radio de un circulo: "))
print("\033[2J\033[1;1f")                                                 # Borrar Pantalla y Situar Cursor
print("\033[;32m" + "Datos del Circulo: ")                                # Escribe en Color verde-claro
print("\033[;33m" + "Radio:   \t"   + str (radio))                        # Escribe en Color amarillo
print("\033[;34m" + "Diametro:\t"   + str (diametro     ( (radio))))      # Escribe en Color azul
print("\033[;35m" + "Area:    \t"   + str (area         ( (radio))))      # Escribe en Color fusia
print("\033[;36m" + "Perimetro:"    + str (perimetro    ( (radio))))      # Escribe en Color Celeste