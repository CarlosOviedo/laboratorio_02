# Ejercicio While.

# Como se puede realizar un DO WHILE (No Exite el DO WHILE EN Python)
'''
While True:
    # Codigo
    # Codigo
    # Codigo
    if CONDICION :
        break       # Break para salir del While.
'''
# Ingresar palabras a una lista hasta que se ingrese una palabra vacia.
# Mostrar las palabras cargadas.
# Mostrar las palabras ordenadas. 
lista = []
palabra =""
while True:
    palabra=input("Ingrese una Palabra (Para finalizar solo aprete enter): ")
    if(palabra==""):
        break
    elif (palabra!=" "): 
        lista.append(palabra)

lista.sort()
print("Las palabras agregadas y ordenadas [a-z] son: ")
for i in range (0,len(lista),1):
    print (lista[i])
    
# Otra Forma de Recorrer con for..
lista.sort(reverse=True)
print("Las palabras agregadas y ordenadas [z-a] son: ")
for item in lista:
    print (item)