# Modulo Circulo
# Funcion para Diametro
# Funcion para Area.
from math import pi

def diametro (radio):
    diametro = radio + radio
    return diametro

def perimetro (radio):
    perimetro = 2 * pi * radio
    return perimetro

def area (radio):
    area = (pi * (radio **2) )
    return area