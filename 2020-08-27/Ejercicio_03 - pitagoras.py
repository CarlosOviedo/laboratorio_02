# Importamos la funcion sqrt de la libreria math
from math import sqrt

def pitagoras (a,b):
    c = sqrt (a**2 + b**2)
    return c

a = float (input ("Ingrese el lado A: "))
b = float (input ("Ingrese el lado B: "))

c = pitagoras (a,b)

print ("El resultado es: " + str (c))