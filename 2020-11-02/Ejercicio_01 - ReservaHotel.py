from datetime import datetime
from datetime import timedelta

from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic
from PyQt5 import QtCore

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-11-02/Ejercicio_01 - ReservaHotel.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.Fecha          = self.calendario.selectedDate().toString("dd-MM-yyyy")
        self.Cnt_Dias       = 0
        self.costo_por_dia  = 0
        self.calendario.selectionChanged.connect(self.Calendario_cambiar_fecha)
        self.Tipo_habitacion.currentIndexChanged.connect(self.on_habitacion_changed)
        self.boton.clicked.connect(self.Boton_on_clicked)
        


    def Calendario_cambiar_fecha(self)    :
         #Tipo QDate
        self.Fecha=(self.calendario.selectedDate()).toString("dd-MM-yyyy")

    def on_habitacion_changed (self, index):
        indice = self.Tipo_habitacion.currentIndex()
        if  indice == 0 :
            self.costo_habitacion.setText("0")
        elif indice == 1 :
            self.costo_habitacion.setText(str(indice*100))
        elif indice == 2:
             self.costo_habitacion.setText(str(indice*100))
 
    def Boton_on_clicked (self):
        Habitacion= self.Tipo_habitacion.currentText()
        self.Cnt_Dias = self.Cantidad_dias.value()
        self.costo_por_dia = float (self.costo_habitacion.text())  

        CheckIn  = datetime.strptime(self.Fecha+" 10:00:00", '%d-%m-%Y %H:%M:%S')
        CheckOut = CheckIn + timedelta(days=self.Cnt_Dias)

        mensaje = 'Va a reservar una habitacion '+ Habitacion+' por '+str(self.Cnt_Dias)+ ' dias' + '\nFecha de Check In: '+ str(CheckIn) + '\nFecha de Check Out: '+ str(CheckOut) + '\nCosto: $ '+ str(self.Cnt_Dias*self.costo_por_dia)
        
        msg = QMessageBox()
        #Titulo
        msg.setWindowTitle("Reserva")
        #Cuerpo
        msg.setText(mensaje)
        #Icono
        msg.setIcon(QMessageBox.Information)
        #Botones
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        respuesta = msg.exec_()
        if respuesta == QMessageBox.Yes:
            msg2 = QMessageBox()
            #Titulo
            msg2.setWindowTitle("Reserva")
            #Cuerpo
            msg2.setText("Su reserva fue realizada. Lo esperamos")
            #Icono
            msg2.setIcon(QMessageBox.Information)
            msg2.exec_()
        elif respuesta == QMessageBox.Cancel:
            msg2 = QMessageBox()
            #Titulo
            msg2.setWindowTitle("Reserva")
            #Cuerpo
            msg2.setText("No se realizo ninguna reserva")
            #Icono
            msg2.setIcon(QMessageBox.Information)
            msg2.exec_()
           
        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  
