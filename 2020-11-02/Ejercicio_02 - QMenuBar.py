from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic
from PyQt5 import QtCore

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-11-02/Ejercicio_02 - QMenuBar.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.actionNuevo.triggered.connect(self.on_nuevo)
        self.actionAbrir.triggered.connect(self.on_abrir)
        self.actionGuardar.triggered.connect(self.on_guardar)
        self.actionSalir.triggered.connect(self.on_salir)

    def on_nuevo(self):
        print ("Nuevo")
    def on_abrir(self):
        print ("Abrir")
    def on_guardar(self):
        print ("Guardar")               
    def on_salir(self):
        print ("Salir")  
        app.quit()      

        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  
