from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic
from PyQt5 import QtCore

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-11-02/Ejercicio_03 - QMenuBarCalculadora.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.actionSuma.triggered.connect(self.on_suma)
        self.actionResta.triggered.connect(self.on_resta)
        self.actionMultiplicacion.triggered.connect(self.on_multiplicacion)
        self.actionCociente.triggered.connect(self.on_division_cociente)
        self.actionResto.triggered.connect(self.on_division_resto)        
        self.actionSalir.triggered.connect(self.on_salir)

    def on_suma(self):
        numero_a = self.numero_a.value()
        numero_b = self.numero_b.value()
        self.resultado.display((numero_a + numero_b))
    
    def on_resta(self):
        numero_a = self.numero_a.value()
        numero_b = self.numero_b.value()
        self.resultado.display((numero_a - numero_b))
    
    def on_multiplicacion(self):
        numero_a = self.numero_a.value()
        numero_b = self.numero_b.value()
        self.resultado.display((numero_a * numero_b))   
    
    def on_division_cociente(self):
        numero_a = self.numero_a.value()
        numero_b = self.numero_b.value()        
        if numero_b == 0:
            msg = QMessageBox()
            #Titulo
            msg.setWindowTitle("Error")
            #Cuerpo
            msg.setText("No se puede dividir por Cero")
            #Icono
            msg.setIcon(QMessageBox.Warning)
            respuesta = msg.exec_()
        else:
            self.resultado.display((numero_a / numero_b)) 
    
    def on_division_resto(self):
        numero_a = self.numero_a.value()
        numero_b = self.numero_b.value()        
        if numero_b == 0:
            msg = QMessageBox()
            #Titulo
            msg.setWindowTitle("Error")
            #Cuerpo
            msg.setText("No se puede dividir por Cero")
            #Icono
            msg.setIcon(QMessageBox.Warning)
            respuesta = msg.exec_()
        else:
            self.resultado.display((numero_a % numero_b))                          
    
    def on_salir(self):
        print ("Salir")  
        app.quit()      

        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  