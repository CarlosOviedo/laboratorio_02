# Class: Canciones.
# Parametro: listado de canciones.
# Metodo:
#   Agregar una cancion.
#   listado_por_interprete: Muestra todas las canciones de un interprete
#   listado_por_estilo: Muestra tdas las canciones de un estilo.
#   quitar: quitar una cancion por titulo e interprete
from Classcancion  import Cancion

class Canciones:
    def __init__ (self):
        self.lista_canciones= []

    def agregar (self,song):
        if type(song)== Cancion:
            self.lista_canciones.append(song)
            print ("Se agrego a lista: ", song)
        else:
            print (song,"No es del Tipo Cancion")
    
    def listar_todas(self):
        for elemento in self.lista_canciones:
            print (elemento)

    def listado_por_interprete (self,interprete):
        for elemento in self.lista_canciones:
            if elemento.interprete == interprete:
                print(elemento.resumen())

    
    def listado_por_estilo (self,estilo):
        for elemento in self.lista_canciones:
            if elemento.estilo_musical.count (estilo)>0:
                print(elemento.resumen())

    
    def quitar_listado (self,titulo,interprete):
        indice = 0
        for elemento in self.lista_canciones:
            if (elemento.interprete == interprete and elemento.titulo == titulo):
                self.lista_canciones.remove(elemento)
        print ("Se elimino de la lista",interprete,"-",titulo)