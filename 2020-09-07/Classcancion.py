# Clase Cancion
# Parametros: titulo,interprete,duracion en segundos, estilo musical.
# Metodos:
#   resumen: devuelve titulo e interprete
#   __str__: devuelve todos los parametros

class Cancion:

    def __init__ (self,titulo,interprete,duracion_segundos,estilo_musical):
        self.titulo             = titulo
        self.interprete         = interprete
        self.duracion_segundos  = duracion_segundos
        self.estilo_musical     = estilo_musical
    
    def resumen (self):
        return (self.interprete + " - " + self.titulo )

    def __str__ (self):
        return '{0} - {1} - {2} - {3}'.format(self.interprete,self.titulo,self.duracion_segundos,self.estilo_musical)
