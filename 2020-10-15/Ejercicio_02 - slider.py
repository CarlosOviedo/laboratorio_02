from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-15/Ejercicio_02 - slider.ui",self)   #Ubicamos la ventana realizado con "designed"
        self.cantidad_libros.valueChanged.connect(self.on_cantidad_libros_changed)
        self.cantidad_pan.valueChanged.connect(self.on_cantidad_pan_changed)
        self.verticalSlider_red.valueChanged.connect(self.backgroundColor_change)
        self.verticalSlider_blue.valueChanged.connect(self.backgroundColor_change)
        self.verticalSlider_green.valueChanged.connect(self.backgroundColor_change)
        self.boton.clicked.connect(self.on_clicked_boton)

    def on_cantidad_libros_changed(self):
       
        precioLibro = self.precio_libros.text()
        if precioLibro != "":
            precioLibro = float (precioLibro)
        else:
            precioLibro = float("0.0")
        self.total_libros.setText(str(precioLibro * self.cantidad_libros.value()))
    
    def on_cantidad_pan_changed (self):
        precioPan = self.precio_pan.text()
        if precioPan != "":
            precioPan = float (self.precio_pan.text())
        else:
            precioPan = float("0.0")
        self.total_pan.setText(str(precioPan * self.cantidad_pan.value()))
    
    def on_clicked_boton(self):
        calculo_libro   = float (self.total_libros.text())
        calculo_pan     = float (self.total_pan.text())
        calculo_total = calculo_libro + calculo_pan
        self.label_total.setText(str(calculo_total))

    def backgroundColor_change (self):
        red     = self.verticalSlider_red.value()
        blue    = self.verticalSlider_blue.value()
        green   = self.verticalSlider_green.value()
        text = "background:rgb("+str(red)+","+str(blue)+","+str(green)+");"
        self.setStyleSheet(text)


app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  