
edades    = [13,12,5,16,23,24,56,45,78,45,39]

#Filtrar par e impar
pares   =[]
impares =[]

# Uso de Funciones Lambda 
par =    (lambda edad: edad % 2 == 0 )  

impar =  (lambda edad: edad % 2 == 1 )  

# Uso de Filtros
pares = list (filter(par,edades))

impares = list (filter(impar,edades))

print ("Edades:  ",edades  )
print ("Par: ",pares )
print ("Impar: ",impares )