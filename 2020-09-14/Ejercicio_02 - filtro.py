# A partir de la siguiente lista: 

edades    = [13,12,5,16,23,24,56,45,78,45,39]


# Implementar el codigo que agrgue a la lista mayores a personas de 18 años o mayor de 18 años
# Implementar el codigo que agrgue a la lista menores a personas de menos de 18 años

mayores   = []
menores   = []

# Implementacion de funcion que retorna True si es edad es mayor/igual a 18
def may_func(edad):
    return edad>=18

# Practica de funcion lambda ... Retorna True si edad es menor a 18
menor =  (lambda edad: edad<18)  

# Usamos la funcion filter a la cual le mandamos que queremos que nos busque 
# Por ej: los mayores de 18 años.
# Convertimos en lista la devolucion de filter ..

mayores = list (filter(may_func,edades))
menores = list (filter(menor,edades))

# Impresion de Listas
print ("Edades:  ",edades  )
print ("Menores: ",menores )
print ("Mayores: ",mayores )