from math import pi

angulos_grados = [90,180,270,45,360]

# Convertir la lista de Angulos en Grados a Angulos en Radianes

def rad_func(ang):
    return ((ang * pi)/180)

angulos_radianes = list (map (rad_func,angulos_grados))

print ("Angulo en Grados - Angulo en Radianes")
for i in range (0,len(angulos_grados)):
    print (angulos_grados[i], " \t - \t ", angulos_radianes[i])


