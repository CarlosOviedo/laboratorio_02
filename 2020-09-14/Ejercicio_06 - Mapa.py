palabras   = ["manzana","banana","auto","casa","hotel"]
longitudes = []
#Implementar con "map" una lista con las longitudes de cada palabra.

def long_fun(word):
    return ( len(word))

longitudes = list (map (long_fun,palabras))

print ("Palabra - \tLongitud")
for i in range (0,len(palabras)):
    print (palabras[i], " - \t ", longitudes[i])
    