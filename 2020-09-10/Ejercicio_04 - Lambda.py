from ClasePersona import Persona

lista = [Persona("Apellido","nombre",44,"333434"),Persona("Apellido","nombre",8,"333434"),Persona("Apellido","nombre",99,"333434")]

# Utilizamos una funcion lambda para ordenar la lista utilizando la edad. y sin 
# llamar la funcion  
lista.sort(key=lambda x: x.edad)
for item in lista:
    print(item)