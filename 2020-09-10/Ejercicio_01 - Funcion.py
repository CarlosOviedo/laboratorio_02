#De esta forma se carga valores por Defecto 
def suma (x=1,y=8): 
    print ("valor de x: ",x)
    print ("valor de y: ",y)
    return x+y

# Si no pongo valores toma los de defecto
print ("La Suma es:", suma())
# Puedo enviarle los valores directamete
print ("La Suma es:", suma(5,4))
# Puedo poner las variables directamente e igualarlas
print ("La Suma es:", suma(x=10,y=3))
# Puedo poner las variables en otro orden
print ("La Suma es:", suma(y=9,x=25))
# Solo un parametro - Toma el otro por defect
print ("La Suma es:", suma(y=9))



# Para cadena de caracteres
def saludo (pais="Argentina"):
    return "Hola, soy de "+ pais

print (saludo ())    
print (saludo ("Chile"))