# Implemetar funciones lambda de: RESTA - MULTIPLICACION - DIVISION - POTENCIA

suma_lambda =  (lambda x,y: x+y)  

resta_lambda = (lambda x,y: x-y)  

multiplicacion_lambda =  (lambda x,y: x*y)  

division_lambda = (lambda x,y: x/y)

potencia_lambda = (lambda x,y: x**y)

x= 3
y = 2

print("La suma entre",x, " e ", y, "es: ", suma_lambda(x,y))
print("La resta entre",x, " e ", y, "es: ", resta_lambda(x,y))
print("La multiplicacion entre",x, " e ", y, "es: ", multiplicacion_lambda(x,y))
print("La division entre",x, " e ", y, "es: ", division_lambda(x,y))
print("La potencia de",x, " elevado a ", y, "es: ", potencia_lambda(x,y))