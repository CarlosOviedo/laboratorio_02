# Pedir al usuario el radio
# Calcular diametro perimetro area
from ClaseCirculo import Circulo

radio = float(input("Ingrese el Radio de un circulo: "))

Circulo = Circulo(radio)


#print("\033[2J\033[1;1f")                                                 # Borrar Pantalla y Situar Cursor
print("Datos del Circulo: ")                                # Escribe en Color verde-claro
#print("Radio:   \t", radio))                        
print("Diametro: \t", Circulo.diametro())      
print("Area:     \t", Circulo.area())      
print("Perimetro:\t" , Circulo.perimetro())      