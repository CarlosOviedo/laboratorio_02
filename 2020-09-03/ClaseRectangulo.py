class Rectangulo:
    def __init__ (self, Alto, Ancho):
        self.Alto = Alto
        self.Ancho = Ancho
        print ("Objeto Rectangulo de: " +  str(self.Alto) + " x " + str(self.Ancho) )

    def perimetro (self):
        return ((2 * self.Alto) + (2 * self.Ancho))
    
    def area (self):
        return (self.Alto * self.Ancho)