from ClaseRectangulo import Rectangulo

Alto  = float(input("Ingrese el Alto de un rectangulo: "))
Ancho = float(input("Ingrese el ancho de un rectangulo: "))

rectangulo = Rectangulo(Alto,Ancho)

print("Datos del Rectangulo: ")                                
                
print("Altura: \t",     rectangulo.Alto)      
print("Base:   \t",     rectangulo.Ancho)      
print("Area:   \t" ,    rectangulo.area())  
print("Perimetro:\t" ,  rectangulo.perimetro())  