# Clase Personas
# Parametro lista_personas
# Metodo:
#   Agregar: Agregar un Objeto persona a la lista
#   mayor: devuelve la persona mayor.
#   menor: devuelve la persona menor.
from ClasePersona import Persona


# La Funcion func_edad devuelve la edad de la persona que se le envia. Es una funcion y no un metodo.
def func_edad(persona):
        return persona.edad



class Personas:
    def __init__ (self):
        self.lista_personas= []
    
    def agregar (self,Persona):
        self.lista_personas.append(Persona)
        print ("Se agrego a lista: ")
        print (Persona)
   
    def EnLista (self):
        return len(self.lista_personas)

    def MayorEdad (self):
        # Busca el Mayor de Edad con el For ...
        edad_mayor  = self.lista_personas[0].edad
        indice = 0
        for i in range (1, len(self.lista_personas)):
            if self.lista_personas[i].edad> edad_mayor:
                edad_mayor  = self.lista_personas[i].edad
                indice = i
        return self.lista_personas[indice]
    
    def MenorEdad (self):
    # Con Sort se Ordena la lista usando la "Key" que le devuelva la funcion func_edad.
        self.lista_personas.sort(Key=func_edad)
        return self.lista_personas[0]
  
        