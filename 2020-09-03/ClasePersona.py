#Clase Persona
#Parametros: nombre,apellido, edad, dni
# Metodos: nombre_completo ()


class Persona:
    def __init__ (self,nombre,apellido,edad,dni):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.dni = dni

    def nombre_completo (self):
        return (self.apellido + ", " + self.nombre)

    def __str__ (self): #Es llamado cada vez q el objeto se llama desde un print, por eso el nombre __str__
        return '{0},{1},{2},{3}'.format(self.nombre,self.apellido,self.edad,self.dni)