from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
from PyQt5 import QtCore

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-29/Ejercicio_03 - Calendario-date.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.calendario.selectionChanged.connect(self.on_cambiar_fecha)



    def on_cambiar_fecha(self)    :
         #Tipo QDate
        self.fecha.setDate(self.calendario.selectedDate())
    

        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  