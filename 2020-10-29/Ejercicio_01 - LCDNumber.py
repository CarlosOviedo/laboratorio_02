from PyQt5.QtWidgets import QMainWindow, QApplication,QMessageBox
from PyQt5 import uic

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-29/Ejercicio_01 - LCDNumber.ui",self)             #Ubicamos la ventana realizado con "designed"
        self.convertir.clicked.connect(self.on_clicked_boton)
    
    def on_clicked_boton(self):
        numero = 0
        
        if self.hexadecimal.isChecked():
            numero = int(self.numero.text(),16)
        elif self.decimal.isChecked():
            numero = int(self.numero.text(),10)           
        elif self.octal.isChecked():
            numero = int(self.numero.text(),8)
        elif self.binario.isChecked():
            numero = int(self.numero.text(),2)  
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("Seleccionar Base")
            msg.setIcon(QMessageBox.Warning)
            msg.exec_()
        
        self.lcdNumber_decimal.display(numero)
        
        self.lcdNumber_octal.setOctMode ()         
        self.lcdNumber_octal.display (numero)
        
        self.lcdNumber_binario.setBinMode ()        
        self.lcdNumber_binario.display (numero)
        
        self.lcdNumber_hexadecimal.setHexMode ()         
        self.lcdNumber_hexadecimal.display (numero)
        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  