from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
from PyQt5 import QtCore

class MiVentana(QMainWindow):                                                   #Creamos Clase Depende de QMainWindow (Clase superior)
                                                                                #Mi ventana Hereda las propiedades de QMainWindow
    def __init__(self):                                                         #Inicializamos Clase
        super().__init__()                                                      #Inicializamos Clase Superior: (Objeto Padre)
                                                                                #super().__init__(): LLama al constructor del Objeto Heredado
        uic.loadUi("2020-10-29/Ejercicio_02 - LCDNumber.ui",self)             #Ubicamos la ventana realizado con "designed"
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.mostrarHora)
        timer.start(1000)
        self.mostrarHora()


    def mostrarHora(self)    :
        horaActual = QtCore.QTime.currentTime()
        horaTexto  = horaActual.toString("hh:mm:ss")
        self.lcdNumber.display(horaTexto)
    

        
app = QApplication([])

win = MiVentana()
win.show()
app.exec_()  